import { JsonArray, JsonValue } from '../types';
import { JsonFunctionDescriptor, JsonFunctionDescriptorParameters } from '../types';
export declare function unnest({ value }: JsonFunctionDescriptorParameters<JsonArray>, begin?: number): JsonValue;
export declare const JsonFunctionUnnest: JsonFunctionDescriptor;
export default JsonFunctionUnnest;
