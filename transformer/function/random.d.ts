import { JsonObject, JsonNumber } from '../types';
import { JsonFunctionDescriptor, JsonFunctionDescriptorParameters } from '../types';
export declare function random({ value, data, init, rename }: JsonFunctionDescriptorParameters<JsonObject>): JsonNumber;
export declare const JsonFunctionRandom: JsonFunctionDescriptor;
export default JsonFunctionRandom;
