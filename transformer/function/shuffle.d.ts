import { JsonArray } from '../types';
import { JsonFunctionDescriptor, JsonFunctionDescriptorParameters } from '../types';
export declare function shuffle({ value }: JsonFunctionDescriptorParameters<JsonArray>, begin?: number): JsonArray;
export declare const JsonFunctionShuffle: JsonFunctionDescriptor;
export default JsonFunctionShuffle;
