import { JsonArray, JsonObject } from '../types';
import { JsonFunctionDescriptor, JsonFunctionDescriptorParameters } from '../types';
export declare function sequence({ value, data, init, rename }: JsonFunctionDescriptorParameters<JsonObject>): JsonArray;
export declare const JsonFunctionSequence: JsonFunctionDescriptor;
export default JsonFunctionSequence;
