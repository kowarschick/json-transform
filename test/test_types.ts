/**
 * @author    Wolfgang L. J. Kowarschick <kowa@hs-augsburg.de>
 * @copyright 2020 © Wolfgang L. J. Kowarschick
 * @license   MIT
 */

import { isJsonString, isJsonNumber, isJsonBoolean, isJsonNull,
         isJsonPrimitive, isJsonArray, isJsonObject, isJsonValue,
         isJsonFunction, isRegExp, isInitMap
       } from '~/types';

test
( '"abc": isJsonString && isJsonPrimitive && isJsonValue',
  () => { const v = "abc";
          expect(isJsonString(v)    && !isJsonNumber(v)   && !isJsonBoolean(v) && !isJsonNull(v) &&
                 isJsonPrimitive(v) && !isJsonArray(v)    && !isJsonObject(v)  &&
                 isJsonValue(v)     && !isJsonFunction(v) && !isRegExp(v)      && !isInitMap(v)
                ).toBe(true);
        }
);

test
( '1: isJsonNumber && isJsonPrimitive && isJsonValue',
  () => { const v = 1;
          expect(!isJsonString(v)   && isJsonNumber(v)    && !isJsonBoolean(v) && !isJsonNull(v) &&
                 isJsonPrimitive(v) && !isJsonArray(v)    && !isJsonObject(v)  &&
                 isJsonValue(v)     && !isJsonFunction(v) && !isRegExp(v)      && !isInitMap(v)
                ).toBe(true);
        }
);

test
( 'true: isJsonBoolean && isJsonPrimitive && isJsonValue',
  () => { const v = true;
          expect(!isJsonString(v)   && !isJsonNumber(v)   && isJsonBoolean(v) && !isJsonNull(v) &&
                 isJsonPrimitive(v) && !isJsonArray(v)    && !isJsonObject(v)  &&
                 isJsonValue(v)     && !isJsonFunction(v) && !isRegExp(v)      && !isInitMap(v)
                ).toBe(true);
        }
);

test
( 'null: isJsonNull && isJsonPrimitive && isJsonValue',
  () => { const v = null;
          expect(!isJsonString(v)   && !isJsonNumber(v)   && !isJsonBoolean(v) && isJsonNull(v) &&
                 isJsonPrimitive(v) && !isJsonArray(v)    && !isJsonObject(v)  &&
                 isJsonValue(v)     && !isJsonFunction(v) && !isRegExp(v)      && !isInitMap(v)
                ).toBe(true);
        }
);

test
( 'undefined: isJsonNull && isJsonPrimitive && isJsonValue',
  () => { const v = undefined;
          expect(!isJsonString(v)   && !isJsonNumber(v)   && !isJsonBoolean(v) && isJsonNull(v) &&
                 isJsonPrimitive(v) && !isJsonArray(v)    && !isJsonObject(v)  &&
                 isJsonValue(v)     && !isJsonFunction(v) && !isRegExp(v)      && !isInitMap(v)
                ).toBe(true);
        }
);

test
( '[1, 2, 3]: isJsonArray && isJsonValue',
  () => { const v = [1,2,3];
          expect(!isJsonString(v)    && !isJsonNumber(v)   && !isJsonBoolean(v) && !isJsonNull(v) &&
                 !isJsonPrimitive(v) && isJsonArray(v)     && !isJsonObject(v)  &&
                 isJsonValue(v)      && !isJsonFunction(v) && !isRegExp(v)      && !isInitMap(v)
                ).toBe(true);
        }
);

test
( '{"a": 1}: isJsonObject && isJsonValue && isInitMap',
  () => { const v = {"a": 1};
          expect(!isJsonString(v)    && !isJsonNumber(v)   && !isJsonBoolean(v) && !isJsonNull(v) &&
                 !isJsonPrimitive(v) && !isJsonArray(v)    && isJsonObject(v)   &&
                 isJsonValue(v)      && !isJsonFunction(v) && !isRegExp(v)      && isInitMap(v)
                ).toBe(true);
        }
);

test
( '() => 1 : isJsonFunction',
  () => { const v = () => 1;
          expect(!isJsonString(v)    && !isJsonNumber(v)  && !isJsonBoolean(v) && !isJsonNull(v) &&
                 !isJsonPrimitive(v) && !isJsonArray(v)   && !isJsonObject(v)  &&
                 !isJsonValue(v)     && isJsonFunction(v) && !isRegExp(v)      && !isInitMap(v)
                ).toBe(true);
        }
);

test
( '/abc/: isRegExp',
  () => { const v = /abc/;
          expect(!isJsonString(v)    && !isJsonNumber(v)   && !isJsonBoolean(v) && !isJsonNull(v) &&
                 !isJsonPrimitive(v) && !isJsonArray(v)    && !isJsonObject(v)  &&
                 !isJsonValue(v)     && !isJsonFunction(v) && isRegExp(v)       && !isInitMap(v)
                ).toBe(true);
        }
);
