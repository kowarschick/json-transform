import { JsonArray, JsonObject } from '../types';
import { JsonFunctionDescriptor, JsonFunctionDescriptorParameters } from '../types';
export declare function duplicate({ value, init, rename }: JsonFunctionDescriptorParameters<JsonObject>): JsonArray;
export declare const JsonFunctionDuplicate: JsonFunctionDescriptor;
export default JsonFunctionDuplicate;
