import { JsonString, JsonNumber } from '../types';
import { JsonFunctionDescriptor, JsonFunctionDescriptorParameters } from '../types';
export declare function level({ level }: JsonFunctionDescriptorParameters<JsonString>): JsonNumber;
export declare const JsonFunctionLevel: JsonFunctionDescriptor;
export default JsonFunctionLevel;
