import { JsonArray, JsonValue } from '../types';
import { JsonFunctionDescriptor, JsonFunctionDescriptorParameters } from '../types';
export declare function count({ value }: JsonFunctionDescriptorParameters<JsonArray>, begin?: number): JsonValue;
export declare const JsonFunctionCount: JsonFunctionDescriptor;
export default JsonFunctionCount;
