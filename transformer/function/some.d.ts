import { JsonArray, JsonValue } from '../types';
import { JsonFunctionDescriptor, JsonFunctionDescriptorParameters } from '../types';
export declare function some({ value }: JsonFunctionDescriptorParameters<JsonArray>, begin?: number): JsonValue;
export declare const JsonFunctionSome: JsonFunctionDescriptor;
export default JsonFunctionSome;
